
public class BankAccount {

	private int accountNumber;
	private int accountBalance;
	private String customerName;
	private String customerEmail;
	private String customerPhoneNumber;
	
	public BankAccount (int accountNumber, int accountBalance, String customerName, String customerEmail, String customerPhoneNumber    ) {
		this.accountNumber = accountNumber;
		this.accountBalance = accountBalance;
		this.customerName = customerName;
		this.customerEmail = customerEmail;
		this.customerEmail = customerEmail;
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;

	}

	public int getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountBalance(int accountBalance) {
		this.accountBalance = accountBalance;
	}

	public int getAccountBalance() {
		return this.accountBalance;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerEmail() {
		return this.customerEmail;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getCustomerPhoneNumber() {
		return this.customerPhoneNumber;
	}

	public void depositFunds(int funds) {

		this.accountBalance += funds;
		System.out.println("Account balance changed. Account balance is: " + accountBalance);
	}

	public void withdrawFunds(int funds) {
		if (accountBalance >= funds) {
			accountBalance = accountBalance - funds;
			System.out.println("Account balance changed. Account balance is: " + accountBalance);
		} else {
			System.out.println("Unsufficient funds.");
		}

	}
}
