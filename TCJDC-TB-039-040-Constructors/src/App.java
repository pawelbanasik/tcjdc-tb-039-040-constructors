
public class App {

	public static void main(String[] args) {
		BankAccount konto1 = new BankAccount(123456789, 200, "Pawel", "512856675", "pawel.banasik@interia.pl" );
		
		
//		konto1.setAccountNumber(123456789);
//		konto1.setAccountBalance(200);
//		konto1.setCustomerName("Pawel");
//		konto1.setCustomerPhoneNumber("512856675");
//		konto1.setCustomerEmail("pawel.banasik@interia.pl");
		
		
		konto1.depositFunds(50);
		konto1.withdrawFunds(30);
		konto1.withdrawFunds(220);
		
		VipCustomer klient0 = new VipCustomer();
		System.out.println(klient0.getName());
		System.out.println(klient0.getCreditLimit());
		System.out.println(klient0.getEmailAddress());
		
		System.out.println();
		
		VipCustomer klient1 = new VipCustomer ("Patryk", 10000);
		System.out.println(klient1.getName());
		System.out.println(klient1.getCreditLimit());
		System.out.println(klient1.getEmailAddress());
		
		System.out.println();
		
		VipCustomer klient2 = new VipCustomer ("Tomek", 2000, "tomek@gmail.com");
		System.out.println(klient2.getName());
		System.out.println(klient2.getCreditLimit());
		System.out.println(klient2.getEmailAddress());
		
	}

}
